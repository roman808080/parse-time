import re

HOURS_IN_DAY = 8
DAYS_IN_WEEK = 5


def find_by_pattern(time_string, pattern):
    hours = re.match(pattern, time_string)

    if not hours or not hours.endpos:
        return 0

    return int(hours.group(0)[:-1])


def parse_time(time_string):
    times = time_string.split(' ')

    step = 0
    hours = 0
    patterns = [r'[0-9]*(w|W)', r'[0-9]*(d|D)', r'[0-9]*(h|H)']
    multiplicators = [DAYS_IN_WEEK * HOURS_IN_DAY, HOURS_IN_DAY, 1]

    while len(times) > step and patterns and multiplicators:
        result = find_by_pattern(times[step], patterns[0])
        if result:
            step += 1
            hours += result * multiplicators[0]

        del patterns[0]
        del multiplicators[0]

    if not patterns and len(times) != step:
        raise Exception('Format error')

    return hours
