from unittest import TestCase
from parse_time import parse_time


class ParseTimeTests(TestCase):
    def test_1h(self):
        self.assertEqual(parse_time('1h'), 1)

    def test_2h(self):
        self.assertEqual(parse_time('2h'), 2)

    def test_1H(self):
        self.assertEqual(parse_time('1H'), 1)

    def test_empty(self):
        with self.assertRaises(Exception) as exc:
            parse_time('')
        self.assertEqual('Format error', str(exc.exception))

    def test_day(self):
        self.assertEqual(parse_time('2d'), 16)

    def test_days_and_ours(self):
        self.assertEqual(parse_time('2d 1h'), 17)

    def test_week(self):
        self.assertEqual(parse_time('1w'), 40)

    def test_week_and_hour(self):
        self.assertEqual(parse_time('1w 1h'), 41)

    def test_week_and_days(self):
        self.assertEqual(parse_time('1w 1d'), 48)

    def test_weeks_days_hours(self):
        self.assertEqual(parse_time('2w 3d 4h'), 108)

    def test_incorrect_times(self):
        with self.assertRaises(Exception) as exc:
            self.assertEqual(parse_time('2h 2h'), 2)
        self.assertEqual('Format error', str(exc.exception))

        with self.assertRaises(Exception) as exc:
            self.assertEqual(parse_time('4h 1w'), 4)
        self.assertEqual('Format error', str(exc.exception))

        with self.assertRaises(Exception) as exc:
            parse_time('4h 1w 5h 5h 5h')
        self.assertEqual('Format error', str(exc.exception))
